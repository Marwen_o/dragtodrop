﻿using System;
using System.Collections.Generic;
using DropingDrops;
namespace DropingDrops
{
	public interface DropGeoCoordinates
	{
		List<double> getdropgeocoordinates(double distance);

	}
}
