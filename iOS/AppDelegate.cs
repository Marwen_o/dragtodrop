﻿using Foundation;
using UIKit;
using DropingDrops;

namespace DropingDrops.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		//public override UIWindow Window { get; set; }
		//UINavigationController navController;
		//ApplicationModelARViewController viewController;

		public override bool FinishedLaunching(UIApplication uiApplication, NSDictionary launchOptions)
		{
			global::Xamarin.Forms.Forms.Init();
			LoadApplication(new App());

			return base.FinishedLaunching(uiApplication,launchOptions);
		}

	}
}
