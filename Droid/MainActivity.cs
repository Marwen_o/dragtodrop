﻿using Android.App;
using Android.OS;

namespace DropingDrops.Droid
{
	[Activity(Label = "Main", MainLauncher = true)]
	public class MainActivity : ListActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.list_startscreen);
			this.StartActivity(typeof(BasicArchitectActivity));
		}
	}
}


